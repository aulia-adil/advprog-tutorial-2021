package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpirit;
import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpiritState;

public abstract class HighSpiritSpell implements Spell {
	protected HighSpirit spirit;

    /**
     * Constructor untuk menjadikan receiver
     * menjadi attribute command
     * @param spirit
     */
    public HighSpiritSpell(HighSpirit spirit) {
        // TODO: Complete Me [DONE]
        this.spirit = spirit;
    }

    @Override
    public void undo() {
        // TODO: Complete Me [DONE]
        if (spirit.getPrevState() == HighSpiritState.ATTACK) spirit.attackStance();
        else if (spirit.getPrevState() == HighSpiritState.SEALED) spirit.seal();
        else if (spirit.getPrevState() == HighSpiritState.DEFEND) spirit.defenseStance();
        else if (spirit.getPrevState() == HighSpiritState.STEALTH) spirit.stealthStance();
    }
}
