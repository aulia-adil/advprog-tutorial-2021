package id.ac.ui.cs.advprog.tutorial2.command.repository;

import id.ac.ui.cs.advprog.tutorial2.command.core.spell.BlankSpell;
import id.ac.ui.cs.advprog.tutorial2.command.core.spell.ChainSpell;
import id.ac.ui.cs.advprog.tutorial2.command.core.spell.Spell;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class ContractSeal {

    private Map<String, Spell> spells;

    //Attribute ini dibuat untuk melakukan undo
    private Spell latestSpell;

    //Attribute ini dibuat agar tidak bisa undo 2x
    private boolean safeUndo;

    public ContractSeal() {
        spells = new HashMap<>();

        //BlankSpell dijadikan default latestSpell
        latestSpell = new BlankSpell();

        //Safeundo dijadikan true agar
        //bisa melewati testcase undo
        safeUndo = true;
    }

    public void registerSpell(Spell spell) {
        spells.put(spell.spellName(), spell);
    }

    /**
     * Invoker mengeksekusi perintah dari client
     * @param spellName
     */
    public void castSpell(String spellName) {
        // TODO: Complete Me [DONE]
        Spell spell = spells.get(spellName);

        //menjadikan spell input sebagai yang "latest"
        latestSpell = spell;
        safeUndo = true;
        spell.cast();
    }

    /**
     * Invoker mengeksekusi perintah undo
     */
    public void undoSpell() {
        // TODO: Complete Me [DONE]
        if (safeUndo){
            latestSpell.undo();
            safeUndo = false;
        }
    }

    public Collection<Spell> getSpells() { return spells.values(); }
}
