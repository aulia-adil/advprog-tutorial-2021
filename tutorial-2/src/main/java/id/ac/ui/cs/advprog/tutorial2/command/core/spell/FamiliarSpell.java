package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.Familiar;
import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.FamiliarState;

public abstract class FamiliarSpell implements Spell {
    protected Familiar familiar;

    // TODO: Complete Me [DONE]

    /**
     * Constructor untuk menjadikan receiver
     * sebagai attribute pada command
     * @param familiar
     */
    public FamiliarSpell(Familiar familiar) {
        this.familiar = familiar;
    }

    /**
     * Method undo() untuk mengulang
     * kembali state receiver sebelumnya
     */
    @Override
    public void undo() {
        // TODO: Complete Me [DONE]
        if (familiar.getPrevState() == FamiliarState.ACTIVE) familiar.summon();
        else if (familiar.getPrevState() == FamiliarState.SEALED) familiar.seal();
    }
}
