package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import com.sun.org.apache.bcel.internal.classfile.Code;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class MyTransformationTest {

    /**
     * Testing Objek MyTransformation Encode Method
     */
    @Test
    void testMyTranformationEncodeMethod() {

        //Input
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        Spell result = new MyTransformation().encode(spell);

        //Output
        String expected = "XfknwfEfsiENE1jsyEytEfEgqfhpxrnymEytEktwljEtzwEx1twi";
        assertEquals(expected, result.getText());
    }

    @Test
    void testMyTranformationEncodeMethodWithKey() {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        Spell result = new MyTransformation(10).encode(spell);

        String expected = "ckps1kJkxnJSJ6ox3J3yJkJlvkmu2ws3rJ3yJpy1qoJy41J26y1n";
        assertEquals(expected, result.getText());
    }

    @Test
    void testMyTranformationDecodeMethod() {
        String text = "XfknwfEfsiENE1jsyEytEfEgqfhpxrnymEytEktwljEtzwEx1twi";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        Spell result = new MyTransformation().decode(spell);

        String expected = "Safira and I went to a blacksmith to forge our sword";
        assertEquals(expected, result.getText());
    }

    @Test
    void testMyTranformationDecodeMethodWithKey() {
        String text = "ckps1kJkxnJSJ6ox3J3yJkJlvkmu2ws3rJ3yJpy1qoJy41J26y1n";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        Spell result = new MyTransformation(10).decode(spell);

        String expected = "Safira and I went to a blacksmith to forge our sword";
        assertEquals(expected, result.getText());
    }
}