package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

// TODO: add tests

@ExtendWith(MockitoExtension.class)
public class BowAdapterTest {
    private Class<?> bowAdapterClass;
    private Class<?> bowClass;

    @BeforeEach
    public void setUp() throws Exception {
        bowAdapterClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter");
        bowClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow");
    }

    @Test
    public void testBowAdapterIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(bowAdapterClass.getModifiers()));
    }

    @Test
    public void testBowAdapterIsAWeapon() {
        Collection<Type> interfaces = Arrays.asList(bowAdapterClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon")));
    }

    @Test
    public void testBowAdapterConstructorReceivesBowAsParameter() {
        Class<?>[] classArg = new Class[1];
        classArg[0] = bowClass;
        Collection<Constructor<?>> constructors = Arrays.asList(
                bowAdapterClass.getDeclaredConstructors());

        assertTrue(constructors.stream()
                .anyMatch(type -> Arrays.equals(
                        type.getParameterTypes(), classArg)));
    }

    @Test
    public void testBowAdapterOverrideNormalAttackMethod() throws Exception {
        Method normalAttack = bowAdapterClass.getDeclaredMethod("normalAttack");

        assertEquals("java.lang.String",
                normalAttack.getGenericReturnType().getTypeName());
        assertEquals(0,
                normalAttack.getParameterCount());
        assertTrue(Modifier.isPublic(normalAttack.getModifiers()));
    }

    @Test
    public void testBowAdapterOverrideChargedAttackMethod() throws Exception {
        Method chargedAttack = bowAdapterClass.getDeclaredMethod("chargedAttack");

        assertEquals("java.lang.String",
                chargedAttack.getGenericReturnType().getTypeName());
        assertEquals(0,
                chargedAttack.getParameterCount());
        assertTrue(Modifier.isPublic(chargedAttack.getModifiers()));
    }

    @Test
    public void testBowAdapterOverrideGetNameMethod() throws Exception {
        Method getName = bowAdapterClass.getDeclaredMethod("getName");

        assertEquals("java.lang.String",
                getName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    @Test
    public void testBowAdapterOverrideGetHolderMethod() throws Exception {
        Method getHolderName = bowAdapterClass.getDeclaredMethod("getHolderName");

        assertEquals("java.lang.String",
                getHolderName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getHolderName.getParameterCount());
        assertTrue(Modifier.isPublic(getHolderName.getModifiers()));
    }

    // TODO: buat test untuk menguji hasil dari pemanggilan method [DONE]

    @InjectMocks
    private BowAdapter bowAdapter;

    @Mock
    private Bow bow;

    @Test
    void testBowAdapterNormalAttackMethod() {

        //Mock process
        when(bow.shootArrow(false)).thenReturn("Default spontaneous shot");

        //Input: normal attack
        String actual = bowAdapter.normalAttack();

        //Output
        verify(bow).shootArrow(false);
        assertEquals("Default spontaneous shot", actual);
    }

    @Test
    void testBowAdapterChargedAttackMethod() {

        //Mock process
        when(bow.shootArrow(true)).thenReturn("Default aim shot");

        //Input 1: Melakukan charged attack mengakibatkan switch mode aim shot
        String actual = bowAdapter.chargedAttack();

        //Output 1: Output dari actual seharusnya demikian
        assertEquals("Aim shot mode ON", actual);

        //Input 2: Melakukan normal attack mengakibatkan melakukan aim shot
        actual = bowAdapter.normalAttack();

        //Output 2: Cek apakah shoot arrow dilakukan dengan true
        verify(bow).shootArrow(true);

        //Cek apakah output dari actual adalah demikian
        assertEquals("Default aim shot", actual);
    }

    @Test
    void testBowAdapterGetNameMethod() {
        //Mock process
        when(bow.getName()).thenReturn("Default");

        //Input
        String actual = bowAdapter.getName();

        //Output
        verify(bow).getName();
        assertEquals("Default", actual);
    }

    @Test
    void testBowAdapterGetHolderNameMethod() {

        //Mock process
        when(bow.getHolderName()).thenReturn("Default");

        //Input
        String actual = bowAdapter.getHolderName();

        //Output
        verify(bow, atLeastOnce()).getHolderName();
        assertEquals("Default", actual);
    }
}
