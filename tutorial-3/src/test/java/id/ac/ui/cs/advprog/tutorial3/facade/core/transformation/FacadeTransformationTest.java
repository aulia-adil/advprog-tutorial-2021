package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.service.FacadeServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import static org.junit.jupiter.api.Assertions.*;

class FacadeTransformationTest {

    //Untuk membuat objek yang akan ditest
    private FacadeTransformation facadeTransformation;

    /**
     * Konstruksi facadeTransformation attribute
     */
    @BeforeEach
    void setUp() {
        facadeTransformation = new FacadeTransformation();
    }

    /**
     * Testing encode method
     */
    @Test
    public void testFacadeTransformationEncodeMethod(){

        //Input
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String actual = facadeTransformation.encode(spell).getText();

        //Output
        assertEquals("%!%|CKvG))M<B:)JKy$BN_i%F:Mwa$aM[|[!|C/G%iBFFHD.&$*L", actual);
    }

    @Test
    public void testFacadeTransformationDecodeMethod(){
        String text = "_M#!;JJB!Gx{X)b==?<D(&qDD_denCKVFD)Z%%w|@qnDBNF;NS%/D";
        Codex codex = RunicCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String actual = facadeTransformation.decode(spell).getText();

        assertEquals("RUYjpL1XXKx50Bj4q3kn4TAZWRZUZZUjxE l3WnlZf8ZlouZxomd5", actual);
    }
}