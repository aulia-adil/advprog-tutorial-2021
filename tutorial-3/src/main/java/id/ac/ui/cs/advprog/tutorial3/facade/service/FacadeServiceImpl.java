package id.ac.ui.cs.advprog.tutorial3.facade.service;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.CodexTranslator;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.AbyssalTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CelestialTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.FacadeTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.Transformation;
import org.springframework.stereotype.Service;

/*
 * Asumsikan kelas ini sebagai kelas Client
*/
@Service
public class FacadeServiceImpl implements FacadeService {

    //Facade Class
    private Transformation transformation = new FacadeTransformation();

    private boolean recentRequestType;
    private String recentRequestValue;

    @Override
    public String encode(String text){

        //Menjadikan input menjadi objek spell dengan Alpha Codex
        Spell spell = new Spell(text, AlphaCodex.getInstance());

        //Mentransformasikan spell (encoding)
        spell = transformation.encode(spell);

        //Mengembalikan teks yang sudah ditransformasi
        return spell.getText();
    }

    @Override
    public String decode(String code){

        //Menjadikan input menjadi objek spell dengan Runic Codex
        Spell spell = new Spell(code, RunicCodex.getInstance());

        //Mentransformasikan spell (decoding)
        spell = transformation.decode(spell);

        //Mengembalikan teks yang sudah ditransformasi
        return spell.getText();
    }

    @Override
    public void setRequestType(String requestType){
        recentRequestType = requestType.equals("encode");
    }

    @Override
    public void setRequestValue(String requestValue){
        recentRequestValue = requestValue;
    }

    @Override
    public boolean isRequestEncode(){
        return recentRequestType;
    }

    @Override
    public String getRequestValue(){
        return recentRequestValue;
    }
}
