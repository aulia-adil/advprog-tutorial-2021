package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me [DONE]

//Adapter Class
public class BowAdapter implements Weapon {
    private Bow bow;

    //Attribut untuk mengecek aim shot mode
    private Boolean aimShotMode;

    public BowAdapter(Bow bow) {
        this.bow = bow;

        //Defaultnya adalah aim shot mode off
        this.aimShotMode = false;
    }

    /**
     * Mengembalikan bow shoot dari atribut bow
     * @return -> feedback shoot arrow dari atribut bow
     */
    @Override
    public String normalAttack() {
        return bow.shootArrow(aimShotMode);
    }

    /**
     * Mengganti aim shot mode dari atribut bow
     * @return -> feedback apakah aim shot mode ON atau OFF
     */
    @Override
    public String chargedAttack() {

        //Ganti menjadi sebaliknya
        aimShotMode = !aimShotMode;

        //Mengembalikan hasil
        String result = aimShotMode ? "ON" : "OFF";
        return "Aim shot mode " + result;
    }

    @Override
    public String getName() {
        return bow.getName();
    }

    /**
     * Mengembalikan nama dari pemilik bow
     * @return -> holder name dari pemilik bow
     */
    @Override
    public String getHolderName() {
        // TODO: complete me [DONE]
        return bow.getHolderName();
    }
}
