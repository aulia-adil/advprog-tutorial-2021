package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

// TODO: Complete me [DONE]
@Service
public class WeaponServiceImpl implements WeaponService {

    @Autowired
    private BowRepository bowRepository;

    @Autowired
    private SpellbookRepository spellbookRepository;

    @Autowired
    private WeaponRepository weaponRepository;

    @Autowired
    private LogRepository logRepository;

    //Untuk melakukan refreshMana dari SpellbookAdapter
    //Menggunakan prinsip observer pattern
    private List<SpellbookAdapter> spellbookSubscriber;

    /**
     * Merupakan prosedur untuk mengkonstruk
     * weaponRepository sehingga bowRepository dan
     * spellbookRepository dapat dijadikan adapter
     * untuk tiap-tiap objeknya dan kemudian
     * dimasukkan ke dalam weaponRepository
     */
    public void constructWeaponRepository() {

        //Deklarasi method untuk list yang menyimpan subsriber
        spellbookSubscriber = new ArrayList<SpellbookAdapter>();

        //Menyimpan tiap-tiap objek bow
        List<Bow> bowList = bowRepository.findAll();

        //Menyimpan tiap-tiap objek spellbook
        List<Spellbook> spellbookList = spellbookRepository.findAll();

        //Memasukan tiap-tiap objek bow ke dalam weaponRepository
        for (Bow bow: bowList){
            weaponRepository.save(new BowAdapter(bow));
        }

        //Memasukan tiap-tiap objek spellbook ke dalam spellbookRepository
        //dan ke dalam spellbookSubsriber
        for (Spellbook spellbook: spellbookList){
            SpellbookAdapter spellbookAdapter = new SpellbookAdapter(spellbook);
            spellbookSubscriber.add(spellbookAdapter);
            weaponRepository.save(spellbookAdapter);
        }
    }

    //Untuk memastikan hanya terjadi 1 kali konstruksi weaponRepository
    private boolean construct = true;

    // TODO: implement me [DONE]

    /**
     * Mengembalikan objek-objek dari weaponRepository yang sudah
     * dikonstruk
     * @return -> list weaponRepository
     */
    @Override
    public List<Weapon> findAll() {
        if(construct){
            construct = false;
            constructWeaponRepository();
        }
        return weaponRepository.findAll();
    }

    // TODO: implement me [DONE]

    /**
     * Mengupdate log dan weaponRepository jika ada input masuk.
     *
     * Asumsi untuk normal attack ditandai 1 dan charged attack ditandai 2
     *
     * Asumsi untuk SpellbookAdapter hanya bisa direfresh jika
     * SpellbookAdapter lain menjadi input.
     *
     * @param weaponName -> nama dari input weaponName yang akan diupdate
     * @param attackType -> attackType merupakan tanda untuk charged atau normal
     */
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon = weaponRepository.findByAlias(weaponName);

        String attack = attackType == 1?
                " (normal attack): " + weapon.normalAttack() :
                " (charged attack): " + weapon.chargedAttack();

        if (weapon instanceof SpellbookAdapter){
            for (SpellbookAdapter spellbookAdapter: spellbookSubscriber){
                spellbookAdapter.refreshMana(weapon, attackType);
            }
        }

        weaponRepository.save(weapon);
        logRepository.addLog(weapon.getHolderName() + " attacked with " +
                weapon.getName() + attack);
    }

    // TODO: implement me [DONE]

    /**
     * Mengembalikan semua log yang ada
     * @return -> feedback log yang ada
     */
    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
