package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me [DONE]
//Adapter Class
public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;

    //Attribut untuk memastikan large spell tidak digunakan beruntun
    private boolean triggerChargedAttack;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;

        //Defaultnya adalah dapat menggunakan large spell
        triggerChargedAttack = true;
    }

    /**
     * Mengembalikan small spell dari atribut spellbook
     * @return -> feedback small spell dari spellbook
     */
    @Override
    public String normalAttack() {
        return spellbook.smallSpell();
    }

    /**
     * Merefresh attribut triggerChargedAttack menjadi true
     * @param weapon -> Yang masuk hanya SpellbookAdapter instance
     * @param attackType -> attackType 1 berarti normal dan 2 berarti charged
     */
    public void refreshMana(Weapon weapon, int attackType){
        if (weapon != this || attackType != 2){
            triggerChargedAttack = true;
        }
    }

    /**
     * Melakukan large spell dari atribut spellbook
     * @return -> feedback dari charged attack untuk spellbook terkait
     */
    @Override
    public String chargedAttack() {

        //Jika atribut ini false, maka tidak melakukan large spell
        if(triggerChargedAttack){

            //Jika masuk field ini, berarti menghentikan spellbook melakukan
            //large spell secara beruntun
            triggerChargedAttack = false;
            return spellbook.largeSpell();
        }
        return "Not enough mana!";
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}
