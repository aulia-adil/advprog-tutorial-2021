package id.ac.ui.cs.advprog.tutorial3.adapter.controller;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.service.WeaponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(path = "/battle")
public class AdapterController {

    @Autowired
    private WeaponService weaponService;

    // TODO: complete me [DONE]
    @RequestMapping(path = "", method = RequestMethod.GET)
    public String battleHome(Model model) {

        //Memasukan list weapon yang ada ke dalam attribute weapons
        model.addAttribute("weapons", weaponService.findAll());

        //Memasukan list log yang ada ke dalam attribute logs
        model.addAttribute("logs", weaponService.getAllLogs());

        //Cukup jelas
        return "adapter/home";
    }

    // TODO: complete me [DONE]

    //Menjadikan request method post sesuai dengan test
    @RequestMapping(path = "/attack", method = RequestMethod.POST)
    public String attackWithWeapon(
        @RequestParam(value = "weaponName") String weaponName,
        @RequestParam(value = "attackType") int attackType) {

        //Memasukan input ke dalam layer service
        weaponService.attackWithWeapon(weaponName, attackType);

        //Cukup jelas
        return "redirect:/battle";
    }
}
