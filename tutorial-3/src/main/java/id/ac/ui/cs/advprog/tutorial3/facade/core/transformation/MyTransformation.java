package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

/**
 * Menggunakan algoritma Caesar Cipher
 */
public class MyTransformation implements Transformation{

    //Untuk menggeser huruf
    private int key;

    /**
     * Konstruktor dengan parameter
     * @param key -> menggeser huruf
     */
    public MyTransformation(int key) {
        this.key = key;
    }

    /**
     * Konstruktor tanpa parameter
     */
    public MyTransformation(){
        this(5);
    }

    /**
     * Menerima input encoding dan mengembalikan hasilnya
     * @param spell -> objek Spell yang akan di-encoding
     * @return -> Spell yang sudah di-encoding
     */
    @Override
    public Spell encode(Spell spell) {
        return process(spell, true);
    }

    /**
     * Menerima input decoding dan mengembalikan hasilnya
     * @param spell -> objek Spell yang akan di-decoding
     * @return -> Spell yang sudah di-decoding
     */
    @Override
    public Spell decode(Spell spell) {
        return process(spell, false);
    }

    /**
     * Proses encoding atau decoding dengan Caesar Cipher
     * @param spell -> Objek spell yang menjadi bahan
     * @param encode -> True jika encoding, False jika sebaliknya
     * @return -> Hasil encoding atau decoding
     */
    private Spell process(Spell spell, boolean encode) {

        //Jika 1, maka selector akan menjadi encoding
        //Jika -1, maka selector akan menjadi decoding
        int selector = encode? 1 : -1;

        //Menyimpan text dari objek spell
        String text = spell.getText();

        //Membuat array char untuk menyimpan hasil
        char[] res = new char[text.length()];

        //Menyimpan codex dari objek spell
        Codex codex = spell.getCodex();

        //Menyimpan size dari semua char yang ada disimpan codex tersebut
        int sizeAllChar = codex.getCharSize();

        //Looping caeesar cipher dimulai
        for (int i = 0; i < text.length(); i++) {

            //Mengambil char dari text
            char oldChar = text.charAt(i);

            //Mengambil index oldChar berdasarkan codex-nya
            int oldCodexIdx = codex.getIndex(oldChar);

            //Menggeser old char sesuai dengan key dan selecter
            int newCodexIdx = (oldCodexIdx + key * selector) % sizeAllChar;

            //Jika ternyata hasil modulo adalah negatif,
            //maka akan dijadikan positif
            if (newCodexIdx < 0) newCodexIdx += sizeAllChar;

            //Mengambil char dari codex berdasarkan index baru
            char newChar = codex.getChar(newCodexIdx);

            //Memasukan hasil char baru ke dalam array char
            res[i] = newChar;
        }

        //Mengembalikan hasil
        return new Spell(new String(res), codex);
    }
}
