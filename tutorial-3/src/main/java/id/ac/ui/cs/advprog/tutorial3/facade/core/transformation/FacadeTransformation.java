package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.CodexTranslator;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

import java.util.*;

public class FacadeTransformation implements Transformation{

    //Untuk BONUS
    private List<Transformation> transformationList;

    public FacadeTransformation() {

        //Hardcode hanya ada pada deklarasi
        transformationList = new ArrayList<>();
        transformationList.add(new AbyssalTransformation());
        transformationList.add(new CelestialTransformation());
        transformationList.add(new MyTransformation());
    }

    @Override
    public Spell encode(Spell spell) {

        //Encoding dimulai
        for (int i = 0; i < transformationList.size(); i++){
            spell = transformationList.get(i).encode(spell);
        }

        //Mengembalikan hasil yang sudah ditranslasi oleh Codex Translator
        return CodexTranslator.translate(spell, RunicCodex.getInstance());
    }

    @Override
    public Spell decode(Spell spell) {

        //Mentranslasikan spell oleh Codex Translator
        spell = CodexTranslator.translate(spell, AlphaCodex.getInstance());

        //Decoding dimulai
        for (int i = transformationList.size()-1; i > -1; i--){
            spell = transformationList.get(i).decode(spell);
        }

        //Mengembalikan spell hasil
        return spell;
    }
}
