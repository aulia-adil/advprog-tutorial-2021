package id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon;

//Service
public class FesteringGreed implements Weapon {

    private String holderName;

    public FesteringGreed(String holderName) {
        this.holderName = holderName;
    }
    @Override
    public String normalAttack() {
        // TODO: complete me [DONE]
        return "Fakultas Ekonomi dan Bisnis Attack";
    }

    @Override
    public String chargedAttack() {
        // TODO: complete me [DONE]
        return "Fakultas Alumni Sukses Attack";
    }

    @Override
    public String getName() {
        return "Festering Greed";
    }

    @Override
    public String getHolderName() { return holderName; }
}
