package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class KnightAdventurer extends Adventurer {


    public KnightAdventurer(Guild guild) {
        this.name = "Knight";
        //ToDo: Complete Me [DONE]
        this.guild = guild;
    }

    //ToDo: Complete Me [DONE]
    @Override
    public void update() {
        getQuests().add(guild.getQuest());
    }

}
