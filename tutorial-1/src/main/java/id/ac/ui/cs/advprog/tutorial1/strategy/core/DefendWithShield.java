package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
    @Override
    public String defend() {
        return "Instagram Layout Defense";
    }

    //ToDo: Complete me [DONE]
    @Override
    public String getType() {
        return "Grid Shield";
    }
}
