package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {


    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        //ToDo: Complete Me [DONE]
        this.guild = guild;
    }

    //ToDo: Complete Me [DONE]
    @Override
    public void update() {
        String qType = guild.getQuestType();
        if(qType.equals("D") || qType.equals("E")){
            getQuests().add(guild.getQuest());
        }
    }

}
