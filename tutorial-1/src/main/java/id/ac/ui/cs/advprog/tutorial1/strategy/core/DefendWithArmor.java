package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
    @Override
    public String defend() {
        return "Component Reusable Defense";
    }

    //ToDo: Complete me [DONE]
    @Override
    public String getType() {
        return "React Armor";
    }
}
