package id.ac.ui.cs.advprog.tutorial1.observer.service;

import id.ac.ui.cs.advprog.tutorial1.observer.core.*;
import id.ac.ui.cs.advprog.tutorial1.observer.repository.QuestRepository;
import id.ac.ui.cs.advprog.tutorial1.observer.core.Adventurer;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GuildServiceImpl implements GuildService {

    private final QuestRepository questRepository;
    private final Guild guild;
    private final Adventurer agileAdventurer;
    private final Adventurer knightAdventurer;
    private final Adventurer mysticAdventurer;

    public GuildServiceImpl(QuestRepository questRepository) {
        this.questRepository = questRepository;
        this.guild = new Guild();
        //ToDo: Complete Me [DONE]

        //Inisiasi objek terlebih dahulu
        agileAdventurer = new AgileAdventurer(guild);
        knightAdventurer = new KnightAdventurer(guild);
        mysticAdventurer = new MysticAdventurer(guild);

        //Guild adalah objek Observable/Publisher,
        //Oleh karena itu dia sedang meng-add para Observer-nya
        guild.add(agileAdventurer);
        guild.add(knightAdventurer);
        guild.add(mysticAdventurer);
    }

    /**
     * Method ini berfungsi untuk memfilter
     * input quest. Jika sudah pernah ada, maka skip.
     * Jika belum, maka Objek Publisher akan memberikan
     * notifikasi kepada Observer-nya.
     * @param quest -> input quest dari controller.
     */
    @Override
    public void addQuest(Quest quest) {

        //questRepository itu fungsinya mengecek apakah
        //quest tersebut pernah disimpan atau tidak.
        //Jika quest pernah disimpan, maka diskip.
        Quest theQuest = questRepository.save(quest);
        if(theQuest != null){
            guild.addQuest(theQuest);
        }
    }

    /**
     * Method ini berfungsi untuk mengembalikan
     * Adventurer yang akan dijadikan output.
     * @return
     */
    @Override
    public List<Adventurer> getAdventurers() {
        List<Adventurer> adventurerList = new ArrayList<Adventurer>();
        adventurerList.add(agileAdventurer);
        adventurerList.add(knightAdventurer);
        adventurerList.add(mysticAdventurer);
        return adventurerList;
    }
}
