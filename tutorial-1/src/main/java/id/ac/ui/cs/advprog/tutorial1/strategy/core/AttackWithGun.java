package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {
    @Override
    public String attack() {
        return "DUAR KEHACK";
    }

    //ToDo: Complete me [DONE]

    @Override
    public String getType() {
        return "Hacker Gun";
    }
}
