package id.ac.ui.cs.advprog.tutorial1.strategy.service;

import id.ac.ui.cs.advprog.tutorial1.strategy.core.*;
import id.ac.ui.cs.advprog.tutorial1.strategy.repository.AdventurerRepository;
import id.ac.ui.cs.advprog.tutorial1.strategy.repository.StrategyRepository;
import org.springframework.stereotype.Service;

@Service
public class AdventurerServiceImpl implements AdventurerService {

    private final AdventurerRepository adventurerRepository;

    private final StrategyRepository strategyRepository;


    public AdventurerServiceImpl(
            AdventurerRepository adventurerRepository,
            StrategyRepository strategyRepository) {

        this.adventurerRepository = adventurerRepository;
        this.strategyRepository = strategyRepository;
    }

    @Override
    public Iterable<Adventurer> findAll() {
        return adventurerRepository.findAll();
    }

    @Override
    public Adventurer findByAlias(String alias) {
        return adventurerRepository.findByAlias(alias);
    }

    @Override
    public void changeStrategy(String alias, String attackType, String defenseType) {
        //ToDo: Complete me [DONE]

        //Cari Objek adventurer dan assign di sini
        Adventurer adventurer = adventurerRepository.findByAlias(alias);

        //Cari Objek attack sesuai dengan input
        AttackBehavior newAttackType = strategyRepository.getAttackBehaviorByType(attackType);
        DefenseBehavior newDefenseType = strategyRepository.getDefenseBehaviorByType(defenseType);

        //Ganti composition di sini
        adventurer.setAttackBehavior(newAttackType);
        adventurer.setDefenseBehavior(newDefenseType);

        //Save pembaharuan tersebut
        adventurerRepository.save(adventurer);
    }

    @Override
    public Iterable<AttackBehavior> getAttackBehaviors() {
        return strategyRepository.getAttackBehaviors();
    }

    @Override
    public Iterable<DefenseBehavior> getDefenseBehaviors() {
        return strategyRepository.getDefenseBehaviors();
    }
}
