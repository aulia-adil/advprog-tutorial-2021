package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
    @Override
    public String attack() {
        return "*Sniff *Sniff Your 2 AM History";
    }

    //ToDo: Complete me [DONE]
    @Override
    public String getType() {
        return "Packet Sniff Sleight-of-Hand";
    }
}
