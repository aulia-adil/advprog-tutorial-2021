package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {


    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        //ToDo: Complete Me [DONE]

        //Karena guild adalah Objek Observable/Publisher
        //Maka Objek Observer ini menyimpan objek guild
        this.guild = guild;
    }

    //ToDo: Complete Me [DONE]
    @Override
    public void update() {

        //Ambil tipenya terdahulu
        String qType = guild.getQuestType();

        //Sesuai spesifikasi
        if(qType.equals("D") || qType.equals("R")){
            getQuests().add(guild.getQuest());
        }
    }

}
