package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
    @Override
    public String defend() {
        return "Layout Solution Defense";
    }

    @Override
    public String getType() {
        return "Flex Barrier";
    }
    //ToDo: Complete me
}
