##Requirement Tutorial 5

1. Membuat one (mata kuliah) to many (mahasiswa) relationship
2. Mahasiswa mempunyai relationship one (mahasiswa) to many (log) dengan log  
3. Attribute log adalah seperti yang ada pada contoh dengan 
   primary key-nya diincrement secara automatis.
4. Log dapat diisi, diupdate, dan dihapus.
5. Laporan pembayaran dapat di-derive dari log-log yang dimiliki mahasiswa 
dengan attribute seperti yang ada pada contoh. Pembayaran diasumsikan adalah 
   uang yang asdos dapatkan pada bulan tersebut.
6. Sebuah log dapat berisikan data lebih singkat dari satu jam
oleh karena itu jam kerja dan pembayarannya juga dijadikan double (asumsi).