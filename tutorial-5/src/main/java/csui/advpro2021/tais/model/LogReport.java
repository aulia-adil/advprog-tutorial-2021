package csui.advpro2021.tais.model;

import lombok.Data;
import lombok.Getter;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.List;

@Data
public class LogReport {
    private String month;
    private double jamKerja;
    private double pembayaran;

    public static List<LogReport> getLogReport(List<Log> logMahasiswa, int startMonth, int endMonth){
        List<LogReport> logReports = new ArrayList<>();
        for (int month = startMonth;month <= endMonth;month++){
            LogReport logReport = new LogReport();
            logReport.setMonth(new DateFormatSymbols().getMonths()[month]);

            double jamKerja = 0;
            for (Log log: logMahasiswa){
                if (log.getEnd().toLocalDateTime().getMonthValue() == month){
                    jamKerja += ((double) log.getEnd().getTime() - (double)log.getStart().getTime())/(60*60*1000);
                }
            }

            logReport.setJamKerja(jamKerja);
            logReport.setPembayaran(350 * jamKerja);
            logReports.add(logReport);
        }
        return logReports;
    }
}
