package csui.advpro2021.tais.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "log")
@Data
@NoArgsConstructor
public class Log{

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id_log")
    private int idLog;

    @Column(name = "start_date", nullable = false)
    private Timestamp start;

    @Column(name = "end_date", nullable = false)
    private Timestamp end;

    @Column(name = "description")
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "npm", nullable = false)
    private Mahasiswa mahasiswa;

    public Log(Mahasiswa mahasiswa, Timestamp start, Timestamp end, String description) {
        this.start = start;
        this.end = end;
        this.description = description;
        this.mahasiswa = mahasiswa;
    }
}
