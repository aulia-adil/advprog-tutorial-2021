package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;

import java.util.List;

public interface MahasiswaService {
    Mahasiswa createMahasiswa(Mahasiswa mahasiswa);

    Iterable<Mahasiswa> getListMahasiswa();

    Mahasiswa getMahasiswaByNPM(String npm);

    Mahasiswa updateMahasiswa(String npm, Mahasiswa mahasiswa);

    void deleteMahasiswaByNPM(String npm);

    Mahasiswa becomeAsdos(String npm, String kodeMatkul);
}
