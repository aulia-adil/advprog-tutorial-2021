package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogReport;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class LogServiceImpl implements LogService{

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private MahasiswaService mahasiswaService;

    @Override
    public Log buatLog(String npm, LocalDateTime start, LocalDateTime end, String desc) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        Log log = new Log(
                mahasiswa,
                Timestamp.valueOf(start),
                Timestamp.valueOf(end),
                desc
                );
        logRepository.save(log);
        return log;
    }

    @Override
    public Log updateLog(String npm, int id, LocalDateTime start, LocalDateTime end, String desc) {
        Log log = logRepository.findByIdLog(id);
        log.setMahasiswa(mahasiswaService.getMahasiswaByNPM(npm));
        log.setStart(Timestamp.valueOf(start));
        log.setEnd(Timestamp.valueOf(end));
        log.setDescription(desc);
        logRepository.save(log);
        return log;
    }

    @Override
    public void deleteLog(int id) {
        logRepository.deleteById(id);
    }

    @Override
    public List<Log> getAllLogByNPM(String npm) {
        List<Log> result = new ArrayList<>();
        List<Log> logList = logRepository.findAll();
        for (Log log: logList){
            if (log.getMahasiswa().getNpm().equals(npm))
                result.add(log);
        }
        return result;
    }

    @Override
    public List<LogReport> getPembayaran(String npm, int sMonth, int eMonth) {
        List<Log> logList = getAllLogByNPM(npm);
        return LogReport.getLogReport(logList, sMonth, eMonth);
    }


}
