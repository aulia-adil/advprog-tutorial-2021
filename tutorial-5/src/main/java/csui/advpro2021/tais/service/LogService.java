package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogReport;
import csui.advpro2021.tais.model.Mahasiswa;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public interface LogService {
    Log buatLog(String npm, LocalDateTime start, LocalDateTime end, String desc);
    Log updateLog(String npm, int id, LocalDateTime start, LocalDateTime end, String desc);
    void deleteLog(int id);
    List<Log> getAllLogByNPM(String npm);
    List<LogReport> getPembayaran(String npm, int sMonth, int eMonth);
}
