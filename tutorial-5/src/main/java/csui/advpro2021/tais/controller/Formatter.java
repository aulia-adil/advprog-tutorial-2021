package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.Log;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
class LogFormat {
    private String npm;
    private int idLog;
    private String start;
    private String end;
    private String desc;

    public LogFormat(Log log) {
        this.npm = log.getMahasiswa().getNpm();
        this.idLog = log.getIdLog();
        this.start = log.getStart().toString();
        this.end = log.getEnd().toString();
        this.desc = log.getDescription();
    }
}

class IsiLogFormat extends LogFormat{
    private String npm;
    IsiLogFormat(Log log){
        super(log);
        this.npm = log.getMahasiswa().getNpm();
    }
}

@Getter
class CekLogFormat{
    private String npm;
    private List<CekLogEl> logs;

    public CekLogFormat(String npm, List<Log> logList) {
        this.npm = npm;
        int incrementer = 1;
        this.logs = new ArrayList<>();
        for (Log log: logList){
            this.logs.add(new CekLogEl(
                    incrementer,
                    log.getIdLog(),
                    log.getStart().toString(),
                    log.getEnd().toString(),
                    log.getDescription()
            ));
            incrementer++;
        }

    }

    @Getter
    @AllArgsConstructor
    class CekLogEl{
        private int no;
        private int idLog;
        private String start;
        private String end;
        private String desc;
    }
}