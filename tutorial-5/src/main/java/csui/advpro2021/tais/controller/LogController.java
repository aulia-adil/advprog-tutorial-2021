package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogReport;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.service.LogService;
import csui.advpro2021.tais.service.MahasiswaService;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RestController
public class LogController {

    @Autowired
    private LogService logService;

    @PostMapping(path = "isi-log")
    public IsiLogFormat isiLog(
            @RequestParam("npm") String npm,
            @RequestParam("start") String start,
            @RequestParam("start-time") String startTime,
            @RequestParam("end") String end,
            @RequestParam("end-time") String endTime,
            @RequestParam(value = "desc", required = false) String desc
    ){
        String[] startArr = start.split("-");
        String[] startTimeArr = startTime.split(":");
        String[] endArr = end.split("-");
        String[] endTimeArr = endTime.split(":");
        Log log = logService.buatLog(
                npm,
                LocalDateTime.of(
                        Integer.parseInt(startArr[2]),
                        Integer.parseInt(startArr[1]),
                        Integer.parseInt(startArr[0]),
                        Integer.parseInt(startTimeArr[0]),
                        Integer.parseInt(startTimeArr[1]),
                        Integer.parseInt(startTimeArr[2]),
                        0),
                LocalDateTime.of(
                        Integer.parseInt(endArr[2]),
                        Integer.parseInt(endArr[1]),
                        Integer.parseInt(endArr[0]),
                        Integer.parseInt(endTimeArr[0]),
                        Integer.parseInt(endTimeArr[1]),
                        Integer.parseInt(endTimeArr[2]),
                        0),
                desc);
        IsiLogFormat logFormat = new IsiLogFormat(log);
        return logFormat;
    }

    @GetMapping("cek-log/{npm}")
    public CekLogFormat cekLog(@PathVariable(value = "npm") String npm){
        List<Log> logList = logService.getAllLogByNPM(npm);
        return new CekLogFormat(npm, logList);
    }

    @DeleteMapping("hapus-log/{id}")
    public String hapusLog(@PathVariable(value = "id") String id){
        logService.deleteLog(Integer.parseInt(id));
        return "BERHASIL";
    }

    @PutMapping("update-log")
    public IsiLogFormat updateLog(
            @RequestParam("npm") String npm,
            @RequestParam("id") String id,
            @RequestParam("start") String start,
            @RequestParam("start-time") String startTime,
            @RequestParam("end") String end,
            @RequestParam("end-time") String endTime,
            @RequestParam(value = "desc", required = false) String desc
    ){
        String[] startArr = start.split("-");
        String[] startTimeArr = startTime.split(":");
        String[] endArr = end.split("-");
        String[] endTimeArr = endTime.split(":");
        Log log = logService.updateLog(
                npm,
                Integer.parseInt(id),
                LocalDateTime.of(
                        Integer.parseInt(startArr[2]),
                        Integer.parseInt(startArr[1]),
                        Integer.parseInt(startArr[0]),
                        Integer.parseInt(startTimeArr[0]),
                        Integer.parseInt(startTimeArr[1]),
                        Integer.parseInt(startTimeArr[2]),
                        0),
                LocalDateTime.of(
                        Integer.parseInt(endArr[2]),
                        Integer.parseInt(endArr[1]),
                        Integer.parseInt(endArr[0]),
                        Integer.parseInt(endTimeArr[0]),
                        Integer.parseInt(endTimeArr[1]),
                        Integer.parseInt(endTimeArr[2]),
                        0),
                desc);
        IsiLogFormat logFormat = new IsiLogFormat(log);
        return logFormat;
    }

    @GetMapping("pembayaran/{npm}/{startMonth}/{endMonth}")
    public List<LogReport> getLogReport(
            @PathVariable("npm") String npm,
            @PathVariable("startMonth") String startMonth,
            @PathVariable("endMonth") String endMonth
    ){
        int sMonth = Integer.parseInt(startMonth);
        int eMonth = Integer.parseInt(endMonth);
        return logService.getPembayaran(npm, sMonth, eMonth);
    }
}

