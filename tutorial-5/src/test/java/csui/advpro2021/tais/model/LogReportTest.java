package csui.advpro2021.tais.model;

import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class LogReportTest {

    @Test
    public void testLogReportMethod(){
        Mahasiswa mahasiswa = new Mahasiswa("0", "tes", "tes", "tes", "tes");
        Log log = new Log(mahasiswa,
                Timestamp.valueOf(LocalDateTime.of(2000, 1, 1, 1, 20, 10, 0)),
                Timestamp.valueOf(LocalDateTime.of(2000, 1, 1, 2, 20, 10, 0)),
                "TES"
        );
        Log log1 = new Log(mahasiswa,
                Timestamp.valueOf(LocalDateTime.of(2000, 2, 1, 1, 20, 10, 0)),
                Timestamp.valueOf(LocalDateTime.of(2000, 2, 1, 2, 20, 10, 0)),
                "TES"
        );
        List<Log> logList = new ArrayList<>();
        logList.add(log);logList.add(log1);
        List<LogReport> logReports = LogReport.getLogReport(logList, 1, 2);
        int jamKerja = 0;
        int pembayaran = 0;
        for (LogReport logReport: logReports){
            jamKerja += logReport.getJamKerja();
            pembayaran += logReport.getPembayaran();
        }
        assertEquals(jamKerja, 2);
        assertEquals(pembayaran, 700);
    }
}