package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogReport;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class LogServiceImplTest {

    @Mock
    private LogRepository logRepository;

    @Mock
    private MahasiswaService mahasiswaService;

    @InjectMocks
    private LogServiceImpl logService;

    private Mahasiswa mahasiswa;
    private LocalDateTime localDateTime;
    private LocalDateTime localDateTime1;
    private Timestamp timestamp;
    private Timestamp timestamp1;
    private Log log;

    @BeforeEach
    void setUp() {
        localDateTime = LocalDateTime.of(
                2000,
                1,
                13,
                12,
                1,
                2,
                0
        );
        localDateTime1 = LocalDateTime.of(
                2000,
                1,
                14,
                13,
                4,
                1,
                0
        );
        timestamp = Timestamp.valueOf(localDateTime);
        timestamp1 = Timestamp.valueOf(localDateTime1);
        mahasiswa = new Mahasiswa("1", "tes", "tes", "tes", "tes");
        log = new Log(mahasiswa, timestamp, timestamp1, "haha");
    }

    @Test
    public void testBuatLogShouldSaveTheLogAndReturnTheLog(){
        when(mahasiswaService.getMahasiswaByNPM("1")).thenReturn(mahasiswa);
        Log log1 = logService.buatLog("1", localDateTime, localDateTime1, "haha");
        verify(logRepository).save(log);
        assertEquals(log, log1);
    }

    @Test
    public void testUpdateLogShouldSaveAndUpdateTheLog(){
        when(mahasiswaService.getMahasiswaByNPM("1")).thenReturn(mahasiswa);
        when(logRepository.findByIdLog(1)).thenReturn(log);
        log.setDescription("hoho");
        Log log1 = logService.updateLog("1", 1, localDateTime, localDateTime1, "hoho");
        verify(logRepository).save(log);
        assertEquals(log, log1);
    }

    @Test
    public void testGetAllLogByNPMMethod(){
        Mahasiswa mahasiswa1 = new Mahasiswa(
                "2", "tes", "tes", "tes", "tes"
        );
        Log log1 = new Log(
                mahasiswa1, timestamp, timestamp1, "haha"
        );
        List<Log> list = new ArrayList<>();
        list.add(log1);list.add(log);
        when(logRepository.findAll()).thenReturn(list);
        List<Log> logList = logService.getAllLogByNPM("1");
        assertEquals(logList.size(), 1);
    }

    @Test
    public void testDeleteLogMethod(){
        logService.deleteLog(1);
        verify(logRepository).deleteById(1);
    }

    @Test
    public void testPembayaranMethod(){
        LogReport logReport = new LogReport();
        logReport.setMonth("February");
        logReport.setPembayaran(8767.402777777777);
        logReport.setJamKerja(25.049722222222222);
        List<LogReport> logReportList = new ArrayList<>();
        logReportList.add(logReport);

        Mahasiswa mahasiswa1 = new Mahasiswa(
                "2", "tes", "tes", "tes", "tes"
        );
        Log log1 = new Log(
                mahasiswa1, timestamp, timestamp1, "haha"
        );
        List<Log> list = new ArrayList<>();
        list.add(log1);list.add(log);
        when(logRepository.findAll()).thenReturn(list);
        List<LogReport> logReports = logService.getPembayaran("1", 1, 1);
        assertEquals(logReportList, logReports);
    }
}