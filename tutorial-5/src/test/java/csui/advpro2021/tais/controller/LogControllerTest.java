package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogReport;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.service.LogServiceImpl;
import csui.advpro2021.tais.service.MahasiswaServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = LogController.class)
class LogControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private LogServiceImpl logService;

    private Log log;
    private LocalDateTime localDateTime;
    private LocalDateTime localDateTime1;

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @BeforeEach
    void setUp() {
        localDateTime = LocalDateTime.of(
                2000,
                1,
                13,
                12,
                1,
                2,
                0
        );
        localDateTime1 = LocalDateTime.of(
                2000,
                1,
                14,
                13,
                4,
                1,
                0
        );
        Mahasiswa mahasiswa = new Mahasiswa("1", "tes", "tes", "tes", "tes");
        this.log = new Log(mahasiswa, Timestamp.valueOf(localDateTime),
                Timestamp.valueOf(localDateTime1),"TES");
    }

    @Test
    public void testIsiLogMethod() throws Exception{
        when(logService.buatLog(
                "1",
                localDateTime,
                localDateTime1,
                "TES")).thenReturn(log);
        MvcResult result = mvc.perform(
                post("/isi-log?npm=1&start=13-01-2000&end=14-01-2000&desc=TES&start-time=12:01:02&end-time=13:04:01")
        ).andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        assertEquals(content, mapToJson(new IsiLogFormat(log)));
    }

    @Test
    public void testCekLogFormatMethod() throws Exception{
        List<Log> logList = new ArrayList<>();
        logList.add(log);
        when(logService.getAllLogByNPM("1")).thenReturn(logList);
        String result = mvc.perform(get("/cek-log/1")).andReturn().getResponse().getContentAsString();
        assertEquals(result, mapToJson(new CekLogFormat("1", logList)));
    }

    @Test
    public void testHapusLogMethod() throws Exception{
        String result = mvc.perform(delete("/hapus-log/1")).andReturn().getResponse().getContentAsString();
        assertEquals(result, "BERHASIL");
    }

    @Test
    public void testUpdateLogMethod() throws Exception{
        log.setDescription("hahaha");
        when(logService.updateLog(
                "1",
                1,
                localDateTime,
                localDateTime1,
                "hahaha"
        )).thenReturn(log);
        mvc.perform(put("/update-log?npm=1&start=13-01-2000&end=14-01-2000&desc=hahaha&start-time=12:01:02&end-time=13:04:01&id=1"))
        .andExpect(jsonPath("$.desc").value("hahaha"));
    }

    @Test
    public void testGetLogReportMethod() throws Exception{
        List<LogReport> logReports = new ArrayList<>();
        LogReport logReport = new LogReport();
        logReport.setMonth("February");
        logReport.setJamKerja(1);
        logReport.setPembayaran(350);
        logReports.add(logReport);
        when(logService.getPembayaran("1", 2, 2)).thenReturn(logReports);
        String result = mvc.perform(get("/pembayaran/1/2/2")).andReturn().getResponse().getContentAsString();
        assertEquals(result, mapToJson(logReports));
    }
}