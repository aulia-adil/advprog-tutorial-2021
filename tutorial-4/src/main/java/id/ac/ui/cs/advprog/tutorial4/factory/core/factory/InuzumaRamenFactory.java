package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;

public class InuzumaRamenFactory implements IngredientFactory{
    //Ingridients:
    //Noodle: Ramen
    //Meat: Pork
    //Topping: Boiled Egg
    //Flavor: Spicy
    @Override
    public Noodle getNoodle() {
        return new Ramen();
    }

    @Override
    public Flavor getFlavor() {
        return new Spicy();
    }

    @Override
    public Meat getMeat() {
        return new Pork();
    }

    @Override
    public Topping getTopping() {
        return new BoiledEgg();
    }
}
