package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MondoUdonFactoryTest {

    private MondoUdonFactory mondoUdonFactory;

    @BeforeEach
    void setUp() {
        mondoUdonFactory = new MondoUdonFactory();
    }

    @Test
    void getNoodle() {
        assertTrue(mondoUdonFactory.getNoodle() instanceof Udon);
    }

    @Test
    void getFlavor() {
        assertTrue(mondoUdonFactory.getFlavor() instanceof Salty);
    }

    @Test
    void getMeat() {
        assertTrue(mondoUdonFactory.getMeat() instanceof Chicken);
    }

    @Test
    void getTopping() {
        assertTrue(mondoUdonFactory.getTopping() instanceof Cheese);
    }
}