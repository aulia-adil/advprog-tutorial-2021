package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MenuTest {

    @Mock
    private Noodle noodle;

    @Mock
    private Meat meat;

    @Mock
    private Topping topping;

    @Mock
    private Flavor flavor;

    @Mock
    private IngredientFactory ingredientFactory;

    @InjectMocks
    private Menu menu;

    @BeforeEach
    void setUp() {
        when(ingredientFactory.getFlavor()).thenReturn(flavor);
        when(ingredientFactory.getMeat()).thenReturn(meat);
        when(ingredientFactory.getNoodle()).thenReturn(noodle);
        when(ingredientFactory.getTopping()).thenReturn(topping);
        menu = new Menu("tes", ingredientFactory);
    }

    @Test
    void getName() {
        assertTrue(menu.getName() instanceof String);
    }

    @Test
    void getNoodle() {
        assertTrue(menu.getNoodle() == noodle);
    }

    @Test
    void getMeat() {
        assertTrue(menu.getMeat() == meat);
    }

    @Test
    void getTopping() {
        assertTrue(menu.getTopping() == topping);
    }

    @Test
    void getFlavor() {
        assertTrue(menu.getFlavor() == flavor);
    }
}