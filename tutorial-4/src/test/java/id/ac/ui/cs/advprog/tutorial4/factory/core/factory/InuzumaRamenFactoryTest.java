package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InuzumaRamenFactoryTest {

    private InuzumaRamenFactory inuzumaRamenFactory;

    @BeforeEach
    void setUp() {
        inuzumaRamenFactory = new InuzumaRamenFactory();
    }

    @Test
    void getNoodle() {
        assertTrue(inuzumaRamenFactory.getNoodle() instanceof Ramen);
    }

    @Test
    void getFlavor() {
        assertTrue(inuzumaRamenFactory.getFlavor() instanceof Spicy);
    }

    @Test
    void getMeat() {
        assertTrue(inuzumaRamenFactory.getMeat() instanceof Pork);
    }

    @Test
    void getTopping() {
        assertTrue(inuzumaRamenFactory.getTopping() instanceof BoiledEgg);
    }
}