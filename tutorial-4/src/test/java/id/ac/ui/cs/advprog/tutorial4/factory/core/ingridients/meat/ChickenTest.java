package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ChickenTest {

    @Test
    void getDescription() {
        Chicken chicken = new Chicken();
        assertTrue(chicken.getDescription() instanceof String);
    }
}