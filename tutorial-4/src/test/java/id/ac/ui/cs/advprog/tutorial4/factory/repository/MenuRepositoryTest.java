package id.ac.ui.cs.advprog.tutorial4.factory.repository;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.*;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MenuRepositoryTest {

    @Mock
    private Menu menu;

    @Mock
    private List<Menu> list;

    @InjectMocks
    private MenuRepository menuRepository;

    @Test
    void getMenus() {
        assertTrue(menuRepository.getMenus().hashCode() == list.hashCode());
    }

    @Test
    void add() {
        menuRepository.add(menu);
        verify(list).add(menu);
    }
}