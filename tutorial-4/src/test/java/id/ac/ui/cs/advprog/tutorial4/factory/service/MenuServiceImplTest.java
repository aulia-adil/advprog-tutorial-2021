package id.ac.ui.cs.advprog.tutorial4.factory.service;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.*;
import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MenuServiceImplTest {

    @Mock
    private MenuRepository repo;

    @InjectMocks
    private MenuServiceImpl menuServiceImpl;

    @BeforeEach
    void setUp() {
        menuServiceImpl = new MenuServiceImpl();
    }

    @Test
    void createMenu() {
        assertTrue(menuServiceImpl.createMenu("tes", "LiyuanSoba") instanceof LiyuanSoba);
        assertTrue(menuServiceImpl.createMenu("tes", "InuzumaRamen") instanceof InuzumaRamen);
        assertTrue(menuServiceImpl.createMenu("tes", "MondoUdon") instanceof MondoUdon);
        assertTrue(menuServiceImpl.createMenu("tes", "SnevnezhaShirataki") instanceof SnevnezhaShirataki);
    }

    @Test
    void getMenus() {
        List<Menu> list = menuServiceImpl.getMenus();
        assertTrue(list.size() == 4);
    }
}