package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UmamiTest {

    @Test
    void getDescription() {
        Umami umami = new Umami();
        assertTrue(umami.getDescription() instanceof String);
    }
}