package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SnevnezhaShiratakiFactoryTest {

    private SnevnezhaShiratakiFactory snevnezhaShiratakiFactory;

    @BeforeEach
    void setUp() {
        snevnezhaShiratakiFactory = new SnevnezhaShiratakiFactory();
    }

    @Test
    void getNoodle() {
        assertTrue(snevnezhaShiratakiFactory.getNoodle() instanceof Shirataki);
    }

    @Test
    void getMeat() {
        assertTrue(snevnezhaShiratakiFactory.getMeat() instanceof Fish);
    }

    @Test
    void getTopping() {
        assertTrue(snevnezhaShiratakiFactory.getTopping() instanceof Flower);
    }

    @Test
    void getFlavor() {
        assertTrue(snevnezhaShiratakiFactory.getFlavor() instanceof Umami);
    }
}