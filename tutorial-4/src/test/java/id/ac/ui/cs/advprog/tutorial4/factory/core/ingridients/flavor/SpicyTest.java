package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SpicyTest {

    @Test
    void getDescription() {
        Spicy spicy = new Spicy();
        assertTrue(spicy.getDescription() instanceof String);
    }
}