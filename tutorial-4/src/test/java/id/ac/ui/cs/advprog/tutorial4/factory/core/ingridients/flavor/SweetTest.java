package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SweetTest {

    @Test
    void getDescription() {
        Sweet sweet = new Sweet();
        assertTrue(sweet.getDescription() instanceof String);
    }
}