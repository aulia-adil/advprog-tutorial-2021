package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.InuzumaRamenFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class InitMenuChild {

    @Test
    public void constructAllChild(){
        new InuzumaRamen("tes");
        new LiyuanSoba("tes");
        new MondoUdon("tes");
        new SnevnezhaShirataki("tes");
    }
}
