package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MushroomTest {

    @Test
    void getDescription() {
        assertTrue((new Mushroom()).getDescription() instanceof String);
    }
}