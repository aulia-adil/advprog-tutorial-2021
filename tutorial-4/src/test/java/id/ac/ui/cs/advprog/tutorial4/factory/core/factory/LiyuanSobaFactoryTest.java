package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LiyuanSobaFactoryTest {

    private LiyuanSobaFactory liyuanSobaFactory;

    @BeforeEach
    void setUp() {
        liyuanSobaFactory = new LiyuanSobaFactory();
    }

    @Test
    void getNoodle() {
        assertTrue(liyuanSobaFactory.getNoodle() instanceof Soba);
    }

    @Test
    void getFlavor() {
        assertTrue(liyuanSobaFactory.getFlavor() instanceof Sweet);
    }

    @Test
    void getMeat() {
        assertTrue(liyuanSobaFactory.getMeat() instanceof Beef);
    }

    @Test
    void getTopping() {
        assertTrue(liyuanSobaFactory.getTopping() instanceof Mushroom);
    }
}