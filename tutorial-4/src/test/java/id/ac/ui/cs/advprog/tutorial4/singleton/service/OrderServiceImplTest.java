package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;

class OrderServiceImplTest {

    private OrderServiceImpl orderService;

    @BeforeEach
    void setUp() {
        orderService = new OrderServiceImpl();
    }

    @Test
    void orderADrinkAndGetDrink() {
        orderService.orderADrink("tes");
        OrderDrink orderDrink = orderService.getDrink();
        assertTrue(orderDrink.getDrink().equals(OrderDrink.getInstance().getDrink()));
    }

    @Test
    void orderAFoodandGetFood() {
        orderService.orderAFood("tes");
        OrderFood orderFood = orderService.getFood();
        assertTrue(orderFood.getFood().equals(OrderFood.getInstance().getFood()));
    }
}