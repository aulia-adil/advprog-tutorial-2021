package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SaltyTest {

    @Test
    void getDescription() {
        Salty salty = new Salty();
        assertTrue(salty.getDescription() instanceof String);
    }
}