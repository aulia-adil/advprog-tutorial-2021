## Lazy Instantiation

Kelebihan: tidak boros memory karena hanya digunakan pada saat dibutuhkan. Kita bisa dibilang baru 
saja menghindari "unnecessary" object creation. 

Kekurangan: memakan lebih banyak waktu dalam menjalankan prosesnya
apalagi jika kasusnya adalah multithreaded. Hal ini menyebabkan adanya
proses locking dalam multithreaded sehingga ada waktu yang terbuang.

## Eager Instantiation

Kelebihan: performance waktu yang lebih baik dibanding lazy instantiation.
Hal ini karena objek tersebut sudah terinisialisasi. Akan sangat cocok digunakan
jika memory objek tersebut tidak berat dan sering digunakan.

Kekurangan: dapat menjadi boros memory dan bootup time apalagi jika objek tersebut jarang-jarang digunakan.

